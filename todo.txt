Todo list:

När jag nästan är färdig med arbetet:
migrera till github och lägg in länk i main.tex

Fråga Mats om hur hemmastudierna påverkar planeringen för ga-arbetet.

Skall förklaring av mitt program vara med i inlämningen?
För: Utfyllnad
Mot: Väldigt lwngt att förklara allt, hör inte riktigt hemma tillsammans med det andra (känns det som).
Räcker det med att lämna länk till ett github repo?





Upplägg på arbetet:


	snabb intro på komplexa tal. (som resultat på andra/tredje-grads ekv.)
		kanske inkludera bilder gjorda med mitt program för att illustrera t.ex. kvadrering av komplexa tal.


	polär form (komplex exponent), bevis samt demo av hur det underlättar multiplikation och divition.
		Visa några komplexa funktioner med mitt program.


	Visa nyttan av komplexa tal i beskrivning av oscillerande fenomen.
	Omformulera (ex. växelström, fjädrar, pendlar, etc.)
		Lösa en diff-eq? är detta utanför eller innanför gränserna av vad mitt arbete bör innehålla.

	ev. Signal-analys
		Fourier transformation. vilken blir enklare för att exponent-lagarna är MYCKET lättare att jobba med änn
			olika trig-formler.


övningar:
1. (räkneregler "kvadratisk" form) Visa att (a+i)^2; r \in R bildar en parabel.
2. (räkneregler polär form) Visa att multiplikation med ett komplext tal representerar en omskalning och en rotation.
3.
