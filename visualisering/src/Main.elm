module Main exposing (..)

import Browser
import Html exposing (Html, text, div, br, button, input)
import Html.Events exposing (onInput, onClick)
import Html.Attributes exposing (..)
import Svg exposing (Svg, circle, line)
import Svg.Attributes exposing (cx, cy, r, fill, x1, x2, y1, y2, strokeWidth, stroke)
import Parser exposing (Parser, (|.), (|=), succeed, symbol, float, spaces, keyword, run, oneOf, lazy)

type alias Complex =  { real : Float
                      , imaginary : Float
                      }

-- Defines the size of the complex plane that we want to examine.
maxVal : Float
maxVal = 7

-- The resolution of the Svg(s) that we will draw in.
res : Float
res = 600
-- ress = resolution string
ress : String
ress = String.fromFloat res

-- Simple function to make a complex number out of two real numbers.
c : Float -> Float -> Complex
c r i = Complex r i

-- define multiplication, addition, subtraction and the function \z -> e ^ z for complex numbers

mul : Complex -> Complex -> Complex
mul x y =
    c   (x.real * y.real - x.imaginary * y.imaginary)
        (x.real * y.imaginary + x.imaginary * y.real)

add : Complex -> Complex -> Complex
add x y = c (x.real + y.real) (x.imaginary + y.imaginary)
sub : Complex -> Complex -> Complex
sub x y = add x (mul (c -1 0) y)

exp : Complex -> Complex
exp z = mul (c (e ^ z.real) 0) (c (cos z.imaginary) (sin z.imaginary))

-- gc is short for ``get coordinate''
-- takes a complex number and gives x and y value used when drawing the number in the complex plane.
gc : Complex -> (String, String)
gc z =
    ( String.fromFloat ((res / 2) * (1 + (z.real / maxVal)))
    , String.fromFloat ((res / 2) * (1 - (z.imaginary / maxVal)))
    )

complexDot_ : String -> Complex -> Svg msg
complexDot_ color z = (\(x, y) -> circle [fill color, cx x, cy y, r "10"] []) (gc z)

complexDot : Complex -> Svg msg
complexDot z = (\(x, y) -> circle [cx x, cy y, r "2"] []) (gc z)

-- Makes a list of complex numbers on a horisontal line.
-- Use 4 dots per unit length to make the line.
hLine : Int -> List Complex
hLine y = List.map  (\x -> c x (toFloat y))
                    (List.map (\x -> toFloat x / 8) (List.range (-8 * round maxVal) (8 * round maxVal)))

-- Makes a list of complex numbers on a vertical line.
-- Uses 4 dots per unit length.
vLine : Int -> List Complex
vLine x = List.map  (\y -> c (toFloat x) y)
    (List.map (\y -> toFloat y / 8)
        (List.range (-8 * round maxVal) (8 * round maxVal)))

-- Makes a list of complex numbers
cGrid : List Complex
cGrid = List.concat
    (   List.map hLine (List.range (round (-1 * maxVal)) (round maxVal))
    ++  List.map vLine (List.range (round (-1 * maxVal)) (round maxVal)))

svgGrid : (Complex -> Complex) -> List (Svg msg)
svgGrid f = [ line  [ x1 "0"
                    , y1 (String.fromFloat (res / 2))
                    , x2 ress
                    , y2 (String.fromFloat (res / 2))
                    , strokeWidth "3"
                    , stroke "Black"] []
            , line  [ y1 "0"
                    , x1 (String.fromFloat (res / 2))
                    , y2 ress
                    , x2 (String.fromFloat (res / 2))
                    , strokeWidth "3"
                    , stroke "Black"] []
            , line  [ x1 ((\(x, y) -> x) (gc (c -1.0 maxVal)))
                    , y1 ((\(x, y) -> y) (gc (c -1.0 maxVal)))
                    , x2 ((\(x, y) -> x) (gc (c -1.0 (-1 * maxVal))))
                    , y2 ((\(x, y) -> y) (gc (c -1.0 (-1 * maxVal))))
                    , strokeWidth "1"
                    , stroke "Grey"] []
            , line  [ x1 ((\(x, y) -> x) (gc (c 1 maxVal)))
                    , y1 ((\(x, y) -> y) (gc (c 1 maxVal)))
                    , x2 ((\(x, y) -> x) (gc (c 1 (-1 * maxVal))))
                    , y2 ((\(x, y) -> y) (gc (c 1 (-1 * maxVal))))
                    , strokeWidth "1"
                    , stroke "Grey"] []
            , line  [ x1 ((\(x, y) -> x) (gc (c maxVal -1)))
                    , y1 ((\(x, y) -> y) (gc (c maxVal -1)))
                    , x2 ((\(x, y) -> x) (gc (c (-1 * maxVal) -1)))
                    , y2 ((\(x, y) -> y) (gc (c (-1 * maxVal) -1)))
                    , strokeWidth "1"
                    , stroke "Grey"] []
            , line  [ x1 ((\(x, y) -> x) (gc (c maxVal 1)))
                    , y1 ((\(x, y) -> y) (gc (c maxVal 1)))
                    , x2 ((\(x, y) -> x) (gc (c (-1 * maxVal) 1)))
                    , y2 ((\(x, y) -> y) (gc (c (-1 * maxVal) 1)))
                    , strokeWidth "1"
                    , stroke "Grey"] []
            ]
    ++  List.map (complexDot << f) cGrid
    ++  [ complexDot_ "Black"   (f (c 0 0))
        , complexDot_ "Green"   (f (c 1 0))
        , complexDot_ "Blue"    (f (c 0 1))
        , complexDot_ "Red"     (f (c 1 1))
        ]

-- the operators we have defined.

-- Pn is short for ``Polish notation''.
-- Can either hold our variable, a complex number or an expression (defined above)
type Pn
    = Z
    | Num Complex
    | Add Pn Pn
    | Sub Pn Pn
    | Mul Pn Pn
    | Exp Pn

type alias Model =
    { expr : Pn
    , func : Complex -> Complex {- This is the function that we will visualize -}
    , input : String
    }

type Msg
    = ParseButton
    | StrChange String

init : () -> (Model, Cmd msg)
init _ =
    ( { expr = Z, func = \n -> mul n n, input = ""}
    , Cmd.none
    )


view : Model -> Html Msg
view model =
    div [style "text-align" "center"]
        [ text "f(z)="
        , input [ value model.input, onInput StrChange ] []
        , button [ onClick ParseButton ] [ text "View function!" ]
        , br [] []
        , br [] []
        , Svg.svg
            [ Svg.Attributes.width ress
            , Svg.Attributes.height ress
            , Svg.Attributes.viewBox ("0 0 " ++ ress ++ " " ++ ress)
            ]
            (svgGrid (\x -> x))
        , Svg.svg
            [ Svg.Attributes.width ress, Svg.Attributes.height ress, Svg.Attributes.viewBox ("0 0 " ++ ress ++ " " ++ ress) ]
            (svgGrid model.func)
        ]

update : Msg -> Model -> (Model, Cmd msg)
update msg model =
    case msg of
        ParseButton ->
            case run pn model.input of
                Ok a ->
                    (   { expr = a
                        , func = makeFunc a
                        , input = model.input}
                    ,   Cmd.none)
                Err _ ->
                    ( model, Cmd.none)
        StrChange s ->
            ( {model | input = s}
            , Cmd.none)


makeFunc : Pn  -> (Complex -> Complex)
makeFunc expr = case expr of
    Z -> \z -> z
    Num z -> (\_ -> z)
    Add a b -> (\z -> add (makeFunc a z) (makeFunc b z))
    Sub a b -> (\z -> sub (makeFunc a z) (makeFunc b z))
    Mul a b -> (\z -> mul (makeFunc a z) (makeFunc b z))
    Exp a -> (\z -> exp (makeFunc a z))



pn : Parser Pn
pn = oneOf [parseExpr, parseR, parseI, parseZ]

parseR : Parser Pn
parseR = succeed (\x -> Num (c x 0))
    |= float

parseI : Parser Pn
parseI = succeed (\x -> Num (c 0 x))
    |. symbol "i"
    |= float

parseZ : Parser Pn
parseZ = succeed Z
    |. keyword "z"

parseExpr : Parser Pn
parseExpr = oneOf
    [ succeed Add
        |. keyword "+"
        |. spaces
        |= lazy (\_ -> pn)
        |. spaces
        |= lazy (\_ -> pn)
        |. spaces
    , succeed Sub
        |. keyword "-"
        |. spaces
        |= lazy (\_ -> pn)
        |. spaces
        |= lazy (\_ -> pn)
        |. spaces
    , succeed Mul
        |. keyword "*"
        |. spaces
        |= lazy (\_ -> pn)
        |. spaces
        |= lazy (\_ -> pn)
        |. spaces
    , succeed Exp
        |. symbol "exp"
        |. spaces
        |= lazy (\_ -> pn)
    ]

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view }
